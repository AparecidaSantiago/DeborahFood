//
//  ViewController.swift
//  DeborahFood
//
//  Created by admin on 07/04/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var itens: Array<Item> = []
    
    @IBOutlet weak var fieldNome: UITextField!
    
    @IBOutlet weak var listaScroll: UIScrollView!
    @IBOutlet weak var lbItens: UILabel!
    @IBOutlet weak var lbTotal: UILabel!

    @IBAction func salvarPedido(_ sender: Any) {
        let nome: String! = fieldNome.text
        
        if nome.characters.count <= 0 {
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let data = formatter.string(from: Date())
        
        var texto = "Nome: \(nome!)\n"
        texto += "Data: \(data)\n\n"
        texto += "Itens:\n\(lbItens.text!)\n"
        texto += "\(lbTotal.text!)"
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let file = "pedido.txt"
            let path = dir.appendingPathComponent(file)
            
            let alert = UIAlertController(title: "Pedido realizado", message: texto, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            do {
                try texto.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                self.present(alert, animated: true, completion: nil)
            } catch {
                print("erro ao salvar")
            }
            
            //teste
            do {
                let arquivo = try String(contentsOf: path, encoding: String.Encoding.utf8)
                print(arquivo)
            } catch {
                print("erro ao ler")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.itens = appDelegate.itens
        
        var itensStr = ""
        var total: Float = 0
        
        for item in itens {
            let format: String = "\u{2022} \(item.description) x \(item.qtd)\n"
            
            itensStr += format
            total += item.preco * Float(item.qtd)
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        let valor = formatter.string(from: total as NSNumber)
        
        lbItens.text = itensStr
        lbItens.sizeToFit()
        lbTotal.text = "Total: \(valor!)"
        lbTotal.sizeToFit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
