//
//  Pedido.swift
//  DeborahFood
//
//  Created by admin on 07/04/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation

class Pedido: NSObject, NSCoding {
    var lista: Array<Item>!
    var data: Date!
    var nome: String!
    
    init(data: Date, nome: String) {
        self.lista = Array<Item>()
        self.nome = nome
        self.data = data
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.lista = aDecoder.decodeObject(forKey: "lista") as! Array<Item>
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.lista, forKey: "lista")
    }
    
    func add(item: Item) {
        self.lista.append(item)
    }
    
    func quantidade() -> Int {
        return self.lista.count
    }
    
    func del(pos: Int) {
        self.lista.remove(at: pos)
    }
    
    func troca(origem: Int, destino: Int) {
        let aux = self.lista[origem]
        self.lista[origem] = self.lista[destino]
        self.lista[destino] = aux;
    }
}
