//
//  Item.swift
//  DeborahFood
//
//  Created by admin on 07/04/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation

class Item: NSObject, NSCoding{
    var nome: String!
    var preco: Float!
    var qtd: Int
    
    override var description: String{
        return self.nome
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.nome = aDecoder.decodeObject(forKey: "nome") as! String
        self.preco = aDecoder.decodeObject(forKey: "preco") as! Float
        self.qtd = aDecoder.decodeObject(forKey: "preco") as! Int
    }
    
    init(nome:String, preco:Float) {
        self.nome = nome
        self.preco = preco
        self.qtd = 0
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.nome, forKey: "nome")
        aCoder.encode(self.preco, forKey: "preco")
        
    }
}
