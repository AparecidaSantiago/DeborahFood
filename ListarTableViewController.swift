//
//  ListarTableViewController.swift
//  DeborahFood
//
//  Created by admin on 07/04/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class ListarTableViewController: UITableViewController {
    var cardapio: Cardapio!
    
    @IBAction func fazerPedido(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let itens = appDelegate.itens
        
        if itens.count <= 0 {
            return
        }
        
        let pedidoView = self.storyboard?.instantiateViewController(withIdentifier: "PedidoView") as! ViewController
        
        self.navigationController?.pushViewController(pedidoView, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.cardapio = appDelegate.cardapio
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.cardapio.quantidade()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celula", for: indexPath) as! ListarTableViewCell
        
        let item = self.cardapio.lista[indexPath.row]
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        let valor = formatter.string(from: item.preco as NSNumber)
        
        cell.lbItem.text = item.description
        cell.lbPreco.text = valor
        self.tableView.layer.borderWidth = 1
        self.tableView.layer.borderColor = UIColor.orange.cgColor
        cell.index = indexPath.row
        
        return cell
    }

}

class ListarTableViewCell: UITableViewCell {
    @IBOutlet weak var lbItem: UILabel!
    @IBOutlet weak var lbPreco: UILabel!
    @IBOutlet weak var lbQtd: UILabel!
    var index: Int = 0
    
    @IBAction func stepperChange(_ sender: UIStepper) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var itens = appDelegate.itens
        let cardapio = appDelegate.cardapio
        
        let value = sender.value
        let item =  cardapio!.lista[index]
        
        
        if value > 0 {
            if !itens.contains(item) {
                itens.append(item)
            }
            
            let i: Int! = itens.index(of: item)
            itens[i].qtd = Int(value)
        }
            
        else if value == 0 {
            var i: Int!
            
            if itens.contains(item) {
                i = itens.index(of: item)
                itens.remove(at: i)
            }
        }
        
        appDelegate.itens = itens
        
        lbQtd.text = "\(Int(value))"
    }
}
